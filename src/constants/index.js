import images from './images';
// const apiPath = 'http://localhost:3000/'
const apiPath = 'http://backendenv.eba-xcmkph3s.eu-west-1.elasticbeanstalk.com/'

const languajes = [
    {
        id: "es",
        descripcion: "Español",
    },
    {
        id: "en",
        descripcion: "English",
    },
]

export { images, apiPath , languajes};