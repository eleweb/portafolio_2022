import './App.css';
import { Main,  MainAdmin } from './container';
import './App.scss'

import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import { store } from './store/store'
import { Provider } from 'react-redux'


const App = () => {
  return (
  <div className='app'>
    <Provider store={store}>
      <Router>
        <Routes>
          <Route path="/admin" element={<MainAdmin />}></Route>
          <Route path="/" element={<Main />}></Route>
        </Routes>
      </Router>
    </Provider>
  </div>
  );
}

export default App;
