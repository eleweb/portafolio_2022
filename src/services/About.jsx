import { apiPath } from "../constants";
import { setToken } from "./Auth"
const axios = require('axios');


export async function getAllAbouts(lang = "") {
    try {
        const response = await axios.get(`${apiPath}abouts?lang=${lang}`);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function createAbout(body) {
    try {
        const config = setToken()
        const response = await axios.post(`${apiPath}abouts`,body,config);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function getOneAbout(id) {
    try {
        const response = await axios.get(`${apiPath}abouts/${id}`);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function updateAbout(id,body) {
    try {
        const config = setToken()
        const response = await axios.patch(`${apiPath}abouts/${id}`,body,config);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function deleteAbout(id) {
    try {
        const config = setToken()
        const response = await axios.delete(`${apiPath}abouts/${id}`,config);
        return response.data;
    } catch (error) {
        return []
    }
}