import { apiPath } from "../constants";
import { setToken } from "./Auth"
const axios = require('axios');

export async function getAllElearning(lang = "") {
    try {
        const response = await axios.get(`${apiPath}elearning?lang=${lang}`);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function createElearning(body) {
    try {
        const config = setToken()
        const response = await axios.post(`${apiPath}elearning`,body,config);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function getOneElearning(id) {
    try {
        const response = await axios.get(`${apiPath}elearning/${id}`);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function updateElearning(id,body) {
    try {
        const config = setToken()
        const response = await axios.patch(`${apiPath}elearning/${id}`,body,config);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function deleteElearning(id) {
    try {
        const config = setToken()
        const response = await axios.delete(`${apiPath}elearning/${id}`,config);
        return response.data;
    } catch (error) {
        return []
    }
}