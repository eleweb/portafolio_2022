import { apiPath } from "../constants";
import { setToken } from "./Auth"
const axios = require('axios');

export async function getAllTestimonials(lang = "") {
    try {
        const response = await axios.get(`${apiPath}testimonials?lang=${lang}`);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function createTestimonial(body) {
    try {
        const config = setToken()
        const response = await axios.post(`${apiPath}testimonials`,body,config);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function getOneTestimonial(id) {
    try {
        const config = setToken()
        const response = await axios.get(`${apiPath}testimonials/${id}`,config);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function updateTestimonial(id,body) {
    try {
        const config = setToken()
        const response = await axios.patch(`${apiPath}testimonials/${id}`,body,config);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function deleteTestimonial(id) {
    try {
        const config = setToken()
        const response = await axios.delete(`${apiPath}testimonials/${id}`,config);
        return response.data;
    } catch (error) {
        return []
    }
}