import { apiPath } from "../constants";
import { setToken } from "./Auth"
const axios = require('axios');

export async function getAllSkills(lang = "") {
    try {
        const response = await axios.get(`${apiPath}skills?lang=${lang}`);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function createSkill(body) {
    try {
        const config = setToken()
        const response = await axios.post(`${apiPath}skills`,body,config);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function getOneSkill(id) {
    try {
        const response = await axios.get(`${apiPath}skills/${id}`);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function updateskill(id,body) {
    try {
        const config = setToken()
        const response = await axios.patch(`${apiPath}skills/${id}`,body,config);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function deleteSkill(id) {
    try {
        const config = setToken()
        const response = await axios.delete(`${apiPath}skills/${id}`,config);
        return response.data;
    } catch (error) {
        return []
    }
}