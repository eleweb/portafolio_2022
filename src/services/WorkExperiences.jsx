import { apiPath } from "../constants";
import { setToken } from "./Auth"
const axios = require('axios');

export async function getAllExperiences(lang = "") {
    try {
        const response = await axios.get(`${apiPath}workexperiences?lang=${lang}`);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function createExperiences(body) {
    try {
        const config = setToken()
        const response = await axios.post(`${apiPath}workexperiences`,body,config);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function getOneExperience(id) {
    try {
        const response = await axios.get(`${apiPath}workexperiences/${id}`);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function updateExperience(id,body) {
    try {
        const config = setToken()
        const response = await axios.patch(`${apiPath}workexperiences/${id}`,body,config);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function deleteExperience(id) {
    try {
        const config = setToken()
        const response = await axios.delete(`${apiPath}workexperiences/${id}`,config);
        return response.data;
    } catch (error) {
        return []
    }
}