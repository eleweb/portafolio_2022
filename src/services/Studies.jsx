import { apiPath } from "../constants";
import { setToken } from "./Auth"
const axios = require('axios');

export async function getAllStudies(lang = "") {
    try {
        const response = await axios.get(`${apiPath}studies?lang=${lang}`);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function createStudies(body) {
    try {
        const config = setToken()
        const response = await axios.post(`${apiPath}studies`,body,config);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function getOneStudie(id) {
    try {
        const response = await axios.get(`${apiPath}studies/${id}`);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function updateStudie(id,body) {
    try {
        const config = setToken()
        const response = await axios.patch(`${apiPath}studies/${id}`,body,config);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function deleteStudie(id) {
    try {
        const config = setToken()
        const response = await axios.delete(`${apiPath}studies/${id}`,config);
        return response.data;
    } catch (error) {
        return []
    }
}