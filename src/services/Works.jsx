import { apiPath } from "../constants";
import { setToken } from "./Auth"
const axios = require('axios');

export async function getAllWorks(lang = "") {
    try {
        const response = await axios.get(`${apiPath}work?lang=${lang}`);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function createWork(body) {
    try {
        const config = setToken()
        const response = await axios.post(`${apiPath}work`,body,config);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function getOneWork(id) {
    try {
        const response = await axios.get(`${apiPath}work/${id}`);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function updateWork(id,body) {
    try {
        const config = setToken()
        const response = await axios.patch(`${apiPath}work/${id}`,body,config);
        return response.data;
    } catch (error) {
        return []
    }
}

export async function deleteWork(id) {
    try {
        const config = setToken()
        const response = await axios.delete(`${apiPath}work/${id}`,config);
        return response.data;
    } catch (error) {
        return []
    }
}