import React, { useEffect, useState } from 'react'
import {  motion } from 'framer-motion'
import { TableElearning } from './components/TableElearning'
import { getAllElearning, createElearning, deleteElearning, updateElearning } from '../../services/Elearning'
import { FormElearning } from './components/FormElearning'
import { Button } from '@mui/material';

export const AdminElearning = ({readonly}) => {
    const [ elearnings, setElearnings ] = useState([])
    const [ showForm, setShowform ]     = useState(false)
    const [ rowObject, setRowobject ]     = useState(null)


    useEffect(() => {
        loadData()
    }, []);

    const showNewForm = () =>{
        setShowform(true)
    }


    const createNewElearning = (body) =>{
        if(readonly) return
        createElearning(body).then((res)=>{
            loadData()
        })
    }

    const updateElearningFunc = (_id,body) =>{
        if(readonly) return
        updateElearning(_id,body).then((res)=>{
            loadData()
        })
    }

    const deleteRow = ({_id}) => () =>{
        if(readonly) return
        deleteElearning(_id).then((res)=>{
            loadData()
        })
    }

    const editRow = (row) => () => {
        setRowobject(row)
    }

    const loadData = () => {
        getAllElearning() //0
        .then((data)=>{
            setElearnings(data)
            setRowobject(null)
            setShowform(false)
        })
    }

    return (
        <>
            <motion.div
                transition={{ duration: 3 }}>
                { (!rowObject && !showForm) && (
                    <>
                        <Button disabled={readonly} onClick={ showNewForm }>
                            Nuevo
                        </Button>
                        <TableElearning 
                            readonly = { readonly }
                            key        = {`table`}
                            elearnings = {elearnings} 
                            editRow    = {editRow}
                            deleteRow       = {deleteRow}/>
                    </>
                )}

                { (rowObject || showForm) && (
                    <FormElearning 
                        readonly = { readonly }
                        key                 = {`form`} 
                        setRowobject        = { setRowobject } 
                        setShowform         = { setShowform } 
                        rowObject           = { rowObject } 
                        updateElearningFunc = { updateElearningFunc }
                        createNewELearning  = { createNewElearning }/>
                )}
            </motion.div>
        </>
    )
}