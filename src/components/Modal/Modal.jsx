import { AnimatePresence, motion } from 'framer-motion'
import React from 'react'
import './Modal.scss'

const backDrop = {
    visible: {
        opacity: 1
    },
    hidden : {
        opacity: 0
    }
}

const dialog = {
    visible: {
        y: "100px",
        opacity: 1,
        transition: { delay: 0.5 }
    },
    hidden : {
        y: "-100vh",
        opacity: 0
    }
}

export const Modal = ({ showModal, setModalToggle }) => {
    return (
        <AnimatePresence exitBeforeEnter>
            { showModal && (
                <motion.div
                    variants  = {backDrop}
                    initial   = "hidden"
                    animate   = "visible" 
                    exit      = "hidden"
                    onClick={()=> setModalToggle(false)}
                    className = 'app_modal-backdrop'>
                    <motion.div
                        variants  = {dialog}
                        initial   = "hidden"
                        animate   = "visible" 
                        className = 'app_modal-window'>
                        <div className="app__modal-dialog">
                            <div className="app__modal-content">
                                <div className="app__testimonial-item app__flex">
                                    {/* <img src={urlFor(testimonials[currentIndex].imageurl)} alt={testimonials[currentIndex].name} /> */}
                                    <div className="app__testimonial-content">
                                    <p className="p-text">Mi texto</p>
                                    <div>
                                        <h4 className="bold-text">Nombre</h4>
                                        <h5 className="p-text">Fecha</h5>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </motion.div>
                </motion.div>
            )}
        </AnimatePresence>
    )
}
