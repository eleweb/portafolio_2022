import React, { useState, useEffect, useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { ButtonGroup, Button, Menu, MenuItem } from '@mui/material'
import { Link } from 'react-router-dom'
import './Navbar.scss'
import { images } from '../../constants'
import { HiMenuAlt4, HiX } from 'react-icons/hi'
import { motion } from 'framer-motion'
import { resumePdfLinkES, resumePdfLinkEN} from '../../services/Auth'
import { useLocation } from 'react-router-dom'
import { languajes } from '../../constants'
import { setLang } from '../../store/langState'
import { updateLangData  } from '../../store/dataState'
import i18n from '../../i18n'

export const Navbar = ({ abouts }) => {
    const [toggle, setToggle]           = useState(false)
    const [isRouteAdmin, setRouteadmin] = useState(false)
    const [ location,  ]                = useState(useLocation())
    const lang                          = useSelector((state) => state.lang.lang)
    const dispatch = useDispatch()

    //Inicio bloque de dropdown
        const [anchorEl, setAnchorEl] = useState(null);
        const open = Boolean(anchorEl);
        const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
        };
        const handleClose = () => {
        setAnchorEl(null);
        };
    //Fin bloque dropdown
  

    const isRouteAdminValidator =useCallback(()=> {
        return location.pathname === '/admin'
    },[location])

    useEffect(() => {
        setRouteadmin(isRouteAdminValidator())
    }, [isRouteAdminValidator]);

    const downloadBlob = () =>{

        const link = document.createElement("a");

        link.href = lang==="es" ? resumePdfLinkES : resumePdfLinkEN;

        document.body.appendChild(link);
        link.dispatchEvent(
            new MouseEvent("click", {
                bubbles: true,
                cancelable: true,
                view: window,
            }),
        );
        document.body.removeChild(link);
    }

    const handleLang = (lang) => () => {
        dispatch(setLang(lang))
        dispatch(updateLangData(lang))
        i18n.changeLanguage(lang)
        handleClose()
    }

    return (
        <nav className='app__navbar'>
            <div className='app__navbar-logo'>
                <img src={ images.logo } alt='logo'/>
                {abouts}
            </div>

            { !isRouteAdmin && (
                <ul className='app__navbar-links'>
                    <li className='app__flex p-text' key={`link-home`}>
                        <div />
                        <a href={ `/#home` }>
                            {i18n.t('home')}
                        </a>
                    </li>
                    <li className='app__flex p-text' key={`link-about`}>
                        <div />
                        <a href={ `/#acerca` }>
                            {i18n.t('about')}
                        </a>
                    </li>
                    <li className='app__flex p-text' key={`link-works`}>
                        <div />
                        <a href={ `/#trabajo` }>
                            {i18n.t('works')}
                        </a>
                    </li>
                    <li className='app__flex p-text' key={`link-skills`}>
                        <div />
                        <a href={ `/#habilidades` }>
                            {i18n.t('skills')}
                        </a>
                    </li>
                    <li className='app__flex p-text' key={`link-testimonials`}>
                        <div />
                        <a href={ `/#testimonios` }>
                            {i18n.t('testimonials')}
                        </a>
                    </li>
                    <li className='app__flex p-text' key={`link-contacto`}>
                        <div />
                        <a href={ `/#contacto` }>
                            {i18n.t('contact')}
                        </a>
                    </li>
                    <li className='app__flex p-text' onClick={downloadBlob} key={`link-download`}>
                        <div />
                        <a href="/#">
                            {i18n.t('download_cv')}
                        </a>
                    </li>

                </ul>
            )}


            <div className="app__navbar-admin">
                <ButtonGroup size="small" variant="text" aria-label="text button group">
                    <Button
                        id="demo-positioned-button"
                        aria-controls={open ? 'demo-positioned-menu' : undefined}
                        aria-haspopup="true"
                        aria-expanded={open ? 'true' : undefined}
                        onClick={handleClick}>
                        { (lang === 'es') ? 'Español' : ''}
                        { (lang === 'en') ? 'English' : ''}
                    </Button>
                    
                    { isRouteAdmin  && (
                        <Link to="/">
                            <Button>Home</Button>
                        </Link> 
                    )}

                    { !isRouteAdmin   && (
                        <Link to="/admin">
                            <Button>Admin Area</Button>
                        </Link> 
                    )}

                    <Menu
                        id              = "demo-positioned-menu"
                        aria-labelledby = "demo-positioned-button"
                        anchorEl        = {anchorEl}
                        open            = {open}
                        onClose         = {handleClose}
                        anchorOrigin    = {{
                            vertical: 'top',
                            horizontal: 'left',
                        }}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'left',
                        }}>
                        {
                            languajes.map((lang, index)=>(
                                <MenuItem onClick={handleLang(lang.id)} key={`${lang}-${index}`}>{lang.descripcion}</MenuItem>
                            ))
                        }
                    </Menu>
                </ButtonGroup>
            </div>

            <div className="app__navbar-menu">
                <HiMenuAlt4 onClick={() => setToggle(true)} />

                {toggle && (
                <motion.div
                    whileInView={{ x: [300, 0] }}
                    transition={{ duration: 0.85, ease: 'easeOut' }}>
                    <HiX onClick={() => setToggle(false)} />
                    <ul>
                        
                    {['home', 'acerca', 'trabajo', 'habilidades','testimonios', 'contacto'].map((item) => (
                        <li key={item}>
                        <a href={`#${item}`} onClick={() => setToggle(false)}>
                            {item}
                        </a>
                        </li>
                    ))}
                        <li key={'item-descarga-cv'}>
                            <Button onClick={downloadBlob}>Descargar CV</Button>
                        </li>
                        <li key={'item-navigate'}>
                            { isRouteAdmin && (
                                <Link to="/">
                                    <Button>Home</Button>
                                </Link>
                            )}

                            { !isRouteAdmin && (
                                <Link to="/admin">
                                    <Button>Admin Area</Button>
                                </Link>
                            )}
                        </li>
                    </ul>
                </motion.div>
                )}
            </div>

        </nav>
    )
}