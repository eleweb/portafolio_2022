import React, { useEffect, useState } from 'react'
import {  motion } from 'framer-motion'
import { TableExperiences } from './components/TableExperiences'
import { FormExperiences } from './components/FormExperiences'
import { getAllExperiences, createExperiences, updateExperience, deleteExperience } from '../../services/WorkExperiences'
import { Button } from '@mui/material';

export const AdminExperiences = ({readonly}) => {
    const [ experiences, setExperiences ] = useState([])
    const [ showForm, setShowform ]     = useState(false)
    const [ rowObject, setRowobject ]     = useState(null)


    useEffect(() => {
        loadData()
    }, []);

    const showNewForm = () =>{
        setShowform(true)
    }

    const create = (body) =>{
        if(readonly) return
        createExperiences(body).then((res)=>{
            loadData()
        })
    }

    const update = (_id,body) =>{
        if(readonly) return
        updateExperience(_id,body).then((res)=>{
            loadData()
        })
    }

    const deleteRow = ({_id}) => () =>{
        if(readonly) return
        deleteExperience(_id).then((res)=>{
            loadData()
        })
    }

    const editRow = (row) => () => {
        setRowobject(row)
    }

    const loadData = () => {
        getAllExperiences() //0
        .then((data)=>{
            setExperiences(data)
            setRowobject(null)
            setShowform(false)
        })
    }

    return (
        <>
            <motion.div
                transition={{ duration: 3 }}>
                { (!rowObject && !showForm) && (
                    <>
                        <Button disabled={readonly} onClick={ showNewForm }>
                            Nuevo
                        </Button>
                        <TableExperiences 
                            readonly={readonly}
                            key       = {`table`}
                            experiences     = { experiences } 
                            editRow   = { editRow }
                            deleteRow = { deleteRow }/>
                    </>
                )}

                { (rowObject || showForm) && (
                    <FormExperiences 
                        readonly={readonly}
                        key          = {`form`} 
                        setRowobject = { setRowobject } 
                        setShowform  = { setShowform } 
                        rowObject    = { rowObject } 
                        update       = { update }
                        create       = { create }/>
                )}
            </motion.div>
        </>
    )
}