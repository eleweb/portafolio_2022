import React, { useEffect, useState } from 'react'
import {  motion } from 'framer-motion'
import { TableSkills } from './components/TableSkills'
import { FormSkills } from './components/FormSkills'
import { getAllSkills, createSkill, updateskill, deleteSkill } from '../../services/Skills'
import { Button } from '@mui/material';

export const AdminSkills = ({ readonly }) => {
    const [ skills, setSkills ] = useState([])
    const [ showForm, setShowform ]     = useState(false)
    const [ rowObject, setRowobject ]     = useState(null)


    useEffect(() => {
        loadData()
    }, []);

    const showNewForm = () =>{
        setShowform(true)
    }

    const create = (body) =>{
        if(readonly) return
        createSkill(body).then((res)=>{
            loadData()
        })
    }

    const update = (_id,body) =>{
        if(readonly) return
        updateskill(_id,body).then((res)=>{
            loadData()
        })
    }

    const deleteRow = ({_id}) => () =>{
        if(readonly) return
        deleteSkill(_id).then((res)=>{
            loadData()
        })
    }

    const editRow = (row) => () => {
        setRowobject(row)
    }

    const loadData = () => {
        getAllSkills() //0
        .then((data)=>{
            setSkills(data)
            setRowobject(null)
            setShowform(false)
        })
    }

    return (
        <>
            <motion.div
                transition={{ duration: 3 }}>
                { (!rowObject && !showForm) && (
                    <>
                        <Button disabled={readonly} onClick={ showNewForm }>
                            Nuevo
                        </Button>
                        <TableSkills 
                            readonly  = { readonly }
                            key       = {`table`}
                            skills    = {skills} 
                            editRow   = {editRow}
                            deleteRow = {deleteRow}/>
                    </>
                )}

                { (rowObject || showForm) && (
                    <FormSkills 
                        readonly     = { readonly }
                        key          = {`form`} 
                        setRowobject = { setRowobject } 
                        setShowform  = { setShowform } 
                        rowObject    = { rowObject } 
                        update       = { update }
                        create       = { create }/>
                )}
            </motion.div>
        </>
    )
}