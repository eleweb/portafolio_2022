import React from 'react'
import { 
    Table, 
    TableBody, 
    TableCell, 
    TableContainer, 
    TableRow, 
    Paper,
    Button,
    ButtonGroup,
    Tooltip
} from '@mui/material'
import { Delete, Edit } from '@mui/icons-material';
import './components.scss'
import {  motion } from 'framer-motion'
import ReactTooltip from 'react-tooltip';

export const TableSkills = ({ skills,editRow,deleteRow,readonly }) => {
    const crudButtons = (row) => {
        return (
            <ButtonGroup variant="contained" size="small" aria-label="small outlined primary button group">
                <Tooltip title="Editar">
                    <Button onClick={ editRow(row) }>
                        <Edit />
                    </Button>
                </Tooltip>
                <Tooltip title="Eliminar">
                    <span>
                        <Button disabled={readonly} onClick={ deleteRow(row) }>
                            <Delete />
                        </Button>
                    </span>
                </Tooltip>
            </ButtonGroup>
        )
    }

    const variants = {
        enter: (direction) => {
            return {
            x: direction > 0 ? 1000 : -1000,
            opacity: 0
            };
        },
        center: {
            zIndex: 1,
            x: 0,
            opacity: 1
        },
        exit: (direction) => {
            return {
            zIndex: 0,
            x: direction < 0 ? 1000 : -1000,
            opacity: 0
            };
        }
    };

    
  return (
    <motion.div
        variants   = {variants}
        initial    = "enter"
        animate    = "center"
        exit       = "exit"
        transition = {{
        x          : { type: "spring", stiffness: 300, damping: 30 },
        opacity    : { duration: 0.2 }
        }}>
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650,maxWidth: 700 }} aria-label="simple table">
                <TableBody>
                    {skills.map((row, index) => (
                        <TableRow
                            key = {row._id}
                            sx  = {{ '&:last-child td, &:last-child th': { border: 0 } }}>
                            <TableCell align="center">{row.titulo}</TableCell>
                            <TableCell align="center">{row.description}</TableCell>
                            <TableCell align="center">
                                <div className="app__row-image mini"
                                    data-tip
                                    data-offset="{'right': 100}"
                                    data-for={row._id}>
                                    <img src={ row.image } alt={row.titulo} />
                                </div>
                                <ReactTooltip
                                    id={row._id}
                                    effect="float"
                                    arrowColor="#fff"
                                    className="skills-tooltip">
                                    <div className="app__row-image">
                                        <img src={ row.image } alt={row.titulo} />
                                    </div>
                                </ReactTooltip>
                            </TableCell>
                            <TableCell align='center'>
                                { crudButtons(row) }
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    </motion.div>
  )
}




