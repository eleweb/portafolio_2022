import React , { useState, useRef, useEffect } from 'react'
import { TextField, Grid,  Button, Tooltip, MenuItem, Select } from '@mui/material';
import {  motion } from 'framer-motion'
import { apiPath } from "../../../constants";
import './components.scss';
import { languajes } from '../../../constants'

export const FormSkills = ({ create, setRowobject, rowObject, setShowform, update, readonly }) => {
    const formProps                         = ['titulo','description','image','lang']
    const [ imagePreview, setImagepreview ] = useState(null)
    const fileInput                         = useRef(null)
    const form                              = useRef(null)
    const [ titulo, setTitulo ]             = useState('')
    const [ description, setDescription ]   = useState('')
    const [ _id, setId ]                    = useState(null)
    const [ lang, setLang ]                 = useState('es')
    useEffect(() => {
        if(rowObject){
            setTitulo(rowObject.titulo)
            setDescription(rowObject.description)
            setImagepreview(rowObject.image)
            setId(rowObject._id)
            setLang(rowObject.lang)
        } else {
            // setImagepreview(`http://localhost:3000/404.jpg`)
            setImagepreview(`${apiPath}404.jpg`)
        }
    }, [rowObject]);


    const handleSubmit = e => {
        e.preventDefault();
        const form = e.target;
        const dataBody = getFormValues(form.elements,formProps)
        if(!_id){
            create(dataBody)
        } else {
            update(_id,dataBody)
        }
    }

    const handleChange = (event) => {
        let urlObject = URL.createObjectURL(event.target.files[0])
        setImagepreview(urlObject)
    }

    const handleClickFile = () => {
        fileInput.current.click()
    }

    const handleCancel = () => {
        setRowobject(null)
        setShowform(false)
    }

    const variants = {
        enter: (direction) => {
            return {
            x: direction > 0 ? 1000 : -1000,
            opacity: 0
            };
        },
        center: {
            zIndex: 1,
            x: 0,
            opacity: 1
        },
        exit: (direction) => {
            return {
            zIndex: 0,
            x: direction < 0 ? 1000 : -1000,
            opacity: 0
            };
        }

        };

    return (
        <motion.div
            variants   = {variants}
            initial    = "enter"
            animate    = "center"
            exit       = "exit"
            className='app__form-container'
            transition = {{
            x          : { type: "spring", stiffness: 300, damping: 30 },
            opacity    : { duration: 0.2 }
            }}>
                <form ref={ form} onSubmit={handleSubmit}>
                    <Grid container  spacing={1}>
                        <Grid item xs={1} />
                        <Grid item xs={6}>
                            <Grid container  spacing={1}>
                                <Grid item xs={8}>
                                    <TextField 
                                        fullWidth
                                        size    = "small"
                                        name = "titulo"
                                        value={ titulo }
                                        label   = "Título"
                                        onChange = {(e)=>{setTitulo(e.target.value)}}
                                        variant = "outlined" />
                                </Grid>
                                <Grid item xs={4}>
                                    <Select
                                        fullWidth
                                        size     = "small"
                                        labelId  = "lang-select"
                                        id       = "lang-select"
                                        name = "lang"
                                        value    = {lang}
                                        onChange = {(e)=>{setLang(e.target.value)}}
                                        label    = "Lang">
                                        {
                                            languajes.map((lang, index)=>(
                                                <MenuItem key={`${lang}-${index}`} value={lang.id}>{lang.descripcion}</MenuItem>
                                            ))
                                        }
                                    </Select>
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField 
                                        fullWidth
                                        size    = 'small'
                                        name    = "description"
                                        multiline
                                        rows    = {4}
                                        value   = { description }
                                        onChange = {(e)=>{setDescription(e.target.value)}}
                                        label   = "Descripción" 
                                        variant =  "outlined" />
                                </Grid>
                                <Grid className='app__submit-row' item xs={12}>
                                    <div className="app__buttons-box">
                                        <Tooltip title="Cancelar">
                                            <Button 
                                                variant = 'outlined' 
                                                onClick = { handleCancel }>
                                                Cancelar
                                            </Button>
                                        </Tooltip>
                                        { !_id && (
                                            <Tooltip title="Eliminar">
                                                <span>
                                                    <Button disabled={readonly} type='submit' variant='outlined'>
                                                        Crear
                                                    </Button>
                                                </span>
                                            </Tooltip>
                                        )}

                                        { _id && (
                                            <Tooltip title="Actualizar">
                                                <span>
                                                    <Button 
                                                        disabled={readonly}
                                                        type='submit' 
                                                        variant='outlined'>
                                                        Actualizar
                                                    </Button>
                                                </span>
                                            </Tooltip>
                                        )}
                                    </div>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={4}>
                            <div className="app__files-container">
                                { imagePreview && (
                                    <div className="app__row-image">
                                        <img src={ imagePreview } alt={`preview-skill`} />
                                    </div>
                                )}
                                <div className="upload">
                                    <input
                                        accept   = "image/*"
                                        style    = {{ display: 'none' }}
                                        id       = "image"
                                        name     = "image"
                                        ref      = {fileInput}
                                        onChange = {handleChange}
                                        type     = "file"/>
                                        <label htmlFor="image">
                                            <Button onClick={handleClickFile} variant="outlined" size="small">
                                                Adjuntar Imagen
                                            </Button>
                                        </label>
                                </div>
                            </div>
                        </Grid>
                        <Grid item xs={1} />
                    </Grid>
                </form>
        </motion.div>
    )
}

const getFormValues = (form,props) => {
    const formData = new FormData();

    props.forEach((prop,index)=>{
        if(prop !== 'image'){
            let dataValue = form[prop].value
            if(dataValue)
                formData.append(prop,dataValue)
        } else {
            let file = form[prop].files[0]
            if(file)
                formData.append(prop,file)
        }
    })

    return formData
}