import React, { useEffect, useState } from 'react'
import {  motion } from 'framer-motion'
import { TableTestimonials } from './components/TableTestimonials'
import { FormTestimonials } from './components/FormTestimonials'
import { getAllTestimonials, createTestimonial, updateTestimonial, deleteTestimonial } from '../../services/Testimonials'
import { Button } from '@mui/material';

export const AdminTestimonials = ({readonly}) => {
    const [ testimonials, setTestimonials ] = useState([])
    const [ showForm, setShowform ]     = useState(false)
    const [ rowObject, setRowobject ]     = useState(null)


    useEffect(() => {
        loadData()
    }, []);

    const showNewForm = () =>{
        setShowform(true)
    }

    const create = (body) =>{
        if(readonly) return
        createTestimonial(body).then((res)=>{
            loadData()
        })
    }

    const update = (_id,body) =>{
        if(readonly) return
        updateTestimonial(_id,body).then((res)=>{
            loadData()
        })
    }

    const deleteRow = ({_id}) => () =>{
        if(readonly) return
        deleteTestimonial(_id).then((res)=>{
            loadData()
        })
    }

    const editRow = (row) => () => {
        setRowobject(row)
    }

    const loadData = () => {
        getAllTestimonials() //0
        .then((data)=>{
            setTestimonials(data)
            setRowobject(null)
            setShowform(false)
        })
    }

    return (
        <>
            <motion.div
                transition={{ duration: 3 }}>
                { (!rowObject && !showForm) && (
                    <>
                        { !readonly && (
                            <Button onClick={ showNewForm }>
                                Nuevo
                            </Button>
                        )}
                        <TableTestimonials
                            readonly={readonly} 
                            key       = {`table`}
                            testimonials    = {testimonials} 
                            editRow   = {editRow}
                            deleteRow = {deleteRow}/>
                    </>
                )}

                { (rowObject || showForm) && (
                    <FormTestimonials 
                        readonly={readonly} 
                        key          = {`form`} 
                        setRowobject = { setRowobject } 
                        setShowform  = { setShowform } 
                        rowObject    = { rowObject } 
                        update       = { update }
                        create       = { create }/>
                )}
            </motion.div>
        </>
    )
}