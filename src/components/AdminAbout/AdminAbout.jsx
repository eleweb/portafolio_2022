import React, { useEffect, useState } from 'react'
import {  motion } from 'framer-motion'
import { TableAbouts } from './components/TableAbouts'
import { FormAbouts } from './components/FormAbouts'
import { getAllAbouts, createAbout, updateAbout, deleteAbout } from '../../services/About'
import { Button } from '@mui/material';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';

export const AdminAbout = ({readonly}) => {
    const [ abouts, setAbouts ] = useState([])
    const [ showForm, setShowform ]     = useState(false)
    const [ rowObject, setRowobject ]     = useState(null)
    const [open, setOpen] = useState(false);


    useEffect(() => {
        loadData()
    }, []);

    const showNewForm = () =>{
        setShowform(true)
    }


    const create = (body) =>{
        if(readonly) return
        setOpen(true)
        createAbout(body).then((res)=>{
            setOpen(false)
            loadData()
        })
    }

    const update = (_id,body) =>{
        if(readonly) return
        setOpen(true)
        updateAbout(_id,body).then((res)=>{
            setOpen(false)
            loadData()
        })
    }

    const deleteRow = ({_id}) => () =>{
        if(readonly) return
        setOpen(false)
        deleteAbout(_id).then((res)=>{
            setOpen(true)
            loadData()
        })
    }

    const editRow = (row) => () => {
        setRowobject(row)
    }

    const loadData = () => {
        setOpen(true)
        getAllAbouts() //0
        .then((data)=>{
            setAbouts(data)
            setRowobject(null)
            setShowform(false)
            setOpen(false)
        })
    }

    return (
        <>
            { open && (
                <Backdrop
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={open}
                onClick={()=>setOpen(false)}>
                <CircularProgress color="inherit" />
                </Backdrop>
            )}
            <motion.div
                transition={{ duration: 3 }}>
                { (!rowObject && !showForm) && (
                    <>
                        <Button disabled={readonly} onClick={ showNewForm }>
                            Nuevo
                        </Button>
                        <TableAbouts 
                            readonly = { readonly }
                            key       = {`table`}
                            abouts    = {abouts} 
                            editRow   = {editRow}
                            deleteRow = {deleteRow}/>
                    </>
                )}

                { (rowObject || showForm) && (
                    <FormAbouts 
                        readonly = { readonly }
                        key          = {`form`} 
                        setRowobject = { setRowobject } 
                        setShowform  = { setShowform } 
                        rowObject    = { rowObject } 
                        update       = { update }
                        create       = { create }/>
                )}
            </motion.div>
        </>
    )
}