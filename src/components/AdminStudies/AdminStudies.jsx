import React, { useEffect, useState } from 'react'
import {  motion } from 'framer-motion'
import { TableStudies } from './components/TableStudies'
import { FormStudies } from './components/FormStudies'
import { getAllStudies, createStudies, updateStudie, deleteStudie } from '../../services/Studies'
import { Button } from '@mui/material';

export const AdminStudies = ({ readonly }) => {
    const [ studies, setStudies ] = useState([])
    const [ showForm, setShowform ]     = useState(false)
    const [ rowObject, setRowobject ]     = useState(null)


    useEffect(() => {
        loadData()
    }, []);

    const showNewForm = () =>{
        setShowform(true)
    }

    const create = (body) =>{
        if(readonly) return
        createStudies(body).then((res)=>{
            loadData()
        })
    }

    const update = (_id,body) =>{
        if(readonly) return
        updateStudie(_id,body).then((res)=>{
            loadData()
        })
    }

    const deleteRow = ({_id}) => () =>{
        if(readonly) return
        deleteStudie(_id).then((res)=>{
            loadData()
        })
    }

    const editRow = (row) => () => {
        setRowobject(row)
    }

    const loadData = () => {
        getAllStudies() //0
        .then((data)=>{
            setStudies(data)
            setRowobject(null)
            setShowform(false)
        })
    }

    return (
        <>
            <motion.div
                transition={{ duration: 3 }}>
                { (!rowObject && !showForm) && (
                    <>
                        <Button disabled={readonly} onClick={ showNewForm }>
                            Nuevo
                        </Button>
                        <TableStudies 
                            readonly = { readonly }
                            key       = {`table`}
                            studies    = {studies} 
                            editRow   = {editRow}
                            deleteRow = {deleteRow}/>
                    </>
                )}

                { (rowObject || showForm) && (
                    <FormStudies 
                        readonly = { readonly }
                        key          = {`form`} 
                        setRowobject = { setRowobject } 
                        setShowform  = { setShowform } 
                        rowObject    = { rowObject } 
                        update       = { update }
                        create       = { create }/>
                )}
            </motion.div>
        </>
    )
}