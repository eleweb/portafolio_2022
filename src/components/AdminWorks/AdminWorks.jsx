import React, { useEffect, useState } from 'react'
import {  motion } from 'framer-motion'
import { TableWorks } from './components/TableWorks'
import { FormWorks } from './components/FormWorks'
import { getAllWorks, createWork, updateWork, deleteWork } from '../../services/Works'
import { Button } from '@mui/material';

export const AdminWorks = ({readonly}) => {
    const [ works, setWorks ] = useState([])
    const [ showForm, setShowform ]     = useState(false)
    const [ rowObject, setRowobject ]     = useState(null)


    useEffect(() => {
        loadData()
    }, []);

    const showNewForm = () =>{
        setShowform(true)
    }

    const create = (body) =>{
        if(readonly) return
        createWork(body).then((res)=>{
            loadData()
        })
    }

    const update = (_id,body) =>{
        if(readonly) return
        updateWork(_id,body).then((res)=>{
            loadData()
        })
    }

    const deleteRow = ({_id}) => () =>{
        if(readonly) return
        deleteWork(_id).then((res)=>{
            loadData()
        })
    }

    const editRow = (row) => () => {
        setRowobject(row)
    }

    const loadData = () => {
        getAllWorks() //0
        .then((data)=>{
            setWorks(data)
            setRowobject(null)
            setShowform(false)
        })
    }

    return (
        <>
            <motion.div
                transition={{ duration: 3 }}>
                { (!rowObject && !showForm) && (
                    <>
                        <Button disabled={readonly} onClick={ showNewForm }>
                            Nuevo
                        </Button>
                        <TableWorks 
                            readonly  = {readonly}
                            key       = {`table`}
                            works     = { works } 
                            editRow   = { editRow }
                            deleteRow = { deleteRow }/>
                    </>
                )}

                { (rowObject || showForm) && (
                    <FormWorks 
                        readonly     = {readonly}
                        key          = {`form`} 
                        setRowobject = { setRowobject } 
                        setShowform  = { setShowform } 
                        rowObject    = { rowObject } 
                        update       = { update }
                        create       = { create }/>
                )}
            </motion.div>
        </>
    )
}