import { configureStore } from '@reduxjs/toolkit'
import dataState from './dataState'
import langState from './langState'

export const store = configureStore({
  reducer: {
    lang: langState,
    data: dataState,
  },
})