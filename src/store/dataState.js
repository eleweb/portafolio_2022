import { createSlice } from '@reduxjs/toolkit'
import { getAllAbouts } from '../services/About'
import { getAllSkills } from '../services/Skills'
import { getAllWorks } from '../services/Works'
import { getAllExperiences } from '../services/WorkExperiences'
import { getAllElearning } from '../services/Elearning'
import { getAllStudies } from '../services/Studies'
// import { getAllTestimonials } from '../services/Testimonials'

const initialState = {
  abouts     : [],
  skills     : [],
  works      : [],
  experiences: [],
  elearning  : [],
  studies    : [],
  testimonials: [],
  isLoading: false
}

export const dataSlice = createSlice({
  name: 'dataStore',
  initialState,
  reducers: {
    setLoading: (state, { payload }) => {
      state.isLoading = payload
    },
    setAbouts: (state, { payload }) => {
      state.abouts = payload
    },
    setSkills: (state, { payload }) => {
        state.skills = payload
    },
    setWorks: (state, { payload }) => {
        state.works = payload
    },
    setExperiences: (state, { payload }) => {
        state.experiences = payload
    },
    setElearning: (state, { payload }) => {
        state.elearning = payload
    },
    setStudies: (state, { payload }) => {
        state.studies = payload
    },
    setTestimonials: (state, { payload }) => {
        state.testimonials = payload
    },
  }
})

// export const { setLang } = dataSlice.actions

export const callAbouts =(lang = '') => 
  async (dispatch) => {
    const res = await getAllAbouts(lang)
    dispatch(dataSlice.actions.setAbouts(res))
};

export const callSkills =(lang = '') => 
  async (dispatch) => {
    const res = await getAllSkills(lang)
    dispatch(dataSlice.actions.setSkills(res))
};

export const callWorks =(lang = '') => 
  async (dispatch) => {
    const res = await getAllWorks(lang)
    dispatch(dataSlice.actions.setWorks(res))
};

export const callExperiences =(lang = '') => 
  async (dispatch) => {
    const res = await getAllExperiences(lang)
    dispatch(dataSlice.actions.setExperiences(res))
};

export const callElearning =(lang = '') => 
  async (dispatch) => {
    const res = await getAllElearning(lang)
    dispatch(dataSlice.actions.setElearning(res))
};

export const callStudies =(lang = '') => 
  async (dispatch) => {
    const res = await getAllStudies(lang)
    dispatch(dataSlice.actions.setStudies(res))
};

export const updateLangData =(lang = '') => 
  async (dispatch) => {
    dispatch(dataSlice.actions.setLoading(true))
    dispatch(dataSlice.actions.setStudies(await getAllStudies(lang)))
    dispatch(dataSlice.actions.setSkills(await getAllSkills(lang)))
    dispatch(dataSlice.actions.setWorks(await getAllWorks(lang)))
    dispatch(dataSlice.actions.setExperiences(await getAllExperiences(lang)))
    dispatch(dataSlice.actions.setElearning(await getAllElearning(lang)))
    dispatch(dataSlice.actions.setLoading(false))
};

export default dataSlice.reducer
