import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  lang: "es",
  abouts: []
}

export const langSlice = createSlice({
  name: 'lang',
  initialState,
  reducers: {
    setLang: (state, { payload }) => {
      state.lang = payload
    },
    // setAbouts: (state, { payload }) => {
    //   state.abouts = payload
    // },
  }
})

export const { setLang } = langSlice.actions

export default langSlice.reducer
