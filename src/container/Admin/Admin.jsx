import React, { useState,useEffect, useCallback } from 'react'
import { AppWrap } from '../../wrapper'
import { Box,Tabs, Tab } from '@mui/material'
import { TabPanel, a11yProps } from '../../common'
import { 
    AdminElearning, 
    AdminAbout, 
    AdminSkills, 
    AdminStudies, 
    AdminTestimonials, 
    AdminWorks,
    AdminExperiences } from '../../components'
import './Admin.scss'
import Fingerprint from '@mui/icons-material/Fingerprint';
import {
    IconButton,
    Tooltip,
    Modal,
    TextField,
    Button,
    Grid
}  from '@mui/material';
import { 
    login, 
    addMinutes, 
    encrypt, 
    checkValidity
 } from '../../services/Auth'
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };


const Admin = () => {
    const [value, setValue]         = useState(0);
    const [ email, setEmail ]       = useState('')
    const [ password, setPassword ] = useState('')
    const [ ownerAccess, setOwneraccess ] = useState(checkValidity())
    const [open, setOpen]           = useState(false);
    const resources = [
        {
            title: "Acerca",
            name: "abouts",
            data : []
        },
        {
            title: "Habilidades",
            name: "skills",
            data : []
        },
        {
            title: "E-learning",
            name: "elearning",
            data : []
        },
        {
            title: "Estudios",
            name: "studies",
            data : []
        },
        {
            title: "Testimonios",
            name: "testimonials",
            data : []
        },
        // {
        //     title: "Usuarios",
        //     name: "users",
        //     data : []
        // },
        {
            title: "Proyectos",
            name: "works",
            data : []
        },
        {
            title: "Experiencias",
            name: "workexperiences",
            data : []
        }
    ];

    const getTime = useCallback(() =>{
        setTimeout(()=>{
            // setOwneraccess(checkValidity())
            getTime()
        },5000)
    },[])
    
    useEffect(() => {
        getTime()
      }, [getTime]);

    const handleChange = (event , newValue) => {
        setValue(newValue);
    };

    const handleSubmit = ()=>{
        login({email,password}).then((res)=>{
            let nextTime = addMinutes(1)
            res.timeOut = nextTime.getTime()
            encrypt('key',res)
            setOwneraccess(checkValidity())
            getTime()
            setOpen(false)
        })
    }

    const handleCloseSession = () =>{
        setOwneraccess(false)
        localStorage.removeItem('key')
    }


    return (
        <>
            <h2 className="head-text head-admin">
                Administracion <span>de Recursos</span>
                    { ownerAccess && (
                        <Tooltip title="Cerrar Sessión">
                            <IconButton  onClick = { handleCloseSession }  aria-label="fingerprint" color="success">
                                <Fingerprint fontSize="large" />
                            </IconButton>
                        </Tooltip>
                    )}

                    { !ownerAccess && (
                        <Tooltip title="Autenticar">
                            <IconButton onClick={()=>setOpen(true)} aria-label="fingerprint" color="secondary">
                                <Fingerprint fontSize="large" />
                            </IconButton>
                        </Tooltip>
                    )}
            </h2>
            <div className="app_admin-container">
                <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                    <Tabs value={value} onChange={handleChange} aria-label="basic tabs">
                        {
                            resources.map((resource,index) => (
                                <Tab key={index} label={resource.title} {...a11yProps(index)} />
                            ))
                        }
                    </Tabs>
                </Box>

                <TabPanel value={value} index={0}>
                    <AdminAbout readonly = { !ownerAccess } />
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <AdminSkills readonly = { !ownerAccess } />
                </TabPanel>
                <TabPanel value={value} index={2}>
                    <AdminElearning readonly = { !ownerAccess } />
                </TabPanel>
                <TabPanel value={value} index={3}>
                    <AdminStudies readonly = { !ownerAccess } />
                </TabPanel>
                <TabPanel value={value} index={4}>
                    <AdminTestimonials readonly = { !ownerAccess }/>
                </TabPanel>
                <TabPanel value={value} index={5}>
                    <AdminWorks readonly = { !ownerAccess } />
                </TabPanel>
                <TabPanel value={value} index={6}>
                    <AdminExperiences readonly = { !ownerAccess } />
                </TabPanel>
            </div>

            <Modal
                open={open}
                onClose={()=> setOpen(false)}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description">
                <Box sx={style}>
                    <Grid container  spacing={1}>
                        <Grid item xs={12}>
                            <TextField 
                                fullWidth
                                size     = "small"
                                name     = "email"
                                value    = { email }
                                label    = "Usuario"
                                onChange = { (e)=>{setEmail( e.target.value )} }
                                variant  = "outlined"/>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField 
                                fullWidth
                                size     = 'small'
                                type = "password"
                                name     = "password"
                                value    = { password }
                                onChange = {(e)=>{setPassword( e.target.value )}}
                                label    = "Contraseña" 
                                variant  = "outlined"/>
                        </Grid>
                        <Grid className='app__submit-row' item xs={ 12 }>
                            <div className="app__buttons-box">
                                <Tooltip title="Cancelar">
                                    <Button 
                                        variant = 'outlined' 
                                        onClick = { ()=> setOpen(false) }>
                                        Cancelar
                                    </Button>
                                </Tooltip>
                                { !false && (
                                    <Tooltip title="Eliminar">
                                        <Button onClick={ handleSubmit } variant='outlined'>
                                            Enviar
                                        </Button>
                                    </Tooltip>
                                )}

                                { false && (
                                    <Tooltip title="Actualizar">
                                        <Button 
                                            type='submit' 
                                            variant='outlined'>
                                            Actualizar
                                        </Button>
                                    </Tooltip>
                                )}
                            </div>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
        </>
    )
}

export default AppWrap(
    Admin,
    'admin',
    'app__whitebg',
);


