import React, { useState, useEffect } from 'react';
import { HiChevronLeft, HiChevronRight } from 'react-icons/hi';
import { useSelector } from 'react-redux'
import { motion } from 'framer-motion';
import { Modal, Box, Button} from '@mui/material';
import { AppWrap, MotionWrap } from '../../wrapper';
import './Testimonials.scss';
import { getAllTestimonials } from '../../services/Testimonials';
import { FormTestimonials } from './components/FormTestimonials';
import { createTestimonial  } from '../../services/Testimonials';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import LinearProgress from '@mui/material/LinearProgress';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLinkedin } from '@fortawesome/free-brands-svg-icons'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { solid, regular, brands } from '@fortawesome/fontawesome-svg-core/import.macro'
const boxStyles = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '90vw',
  maxWidth: '95vw',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  borderRadius: '5px',
  p: 4,
};

const Testimonial = () => {
  const [currentIndex, setCurrentIndex]  = useState(0);
  const [testimonials, setTestimonials]  = useState([]);
  const [modalResena, setModalresena]    = useState(false);
  const [open, setOpen] = useState(false);
  const [sucessFeedback, setSuccessfeedback] = useState(false);
  const [statusLoader, setStatusloader] = useState(false);
  const [statusSnackbar, setStatussnackbar] = useState(false);
  const lang                             = useSelector((state) => state.lang.lang);

  const handleClick = (index) => {
    setCurrentIndex(index);
  };


  useEffect(() => {
    loadData()
  }, []);

  const create = (body) =>{
    setStatusloader(true)
    createTestimonial(body).then((res)=>{
        setStatusloader(false)
        setModalresena(false)
        setSuccessfeedback(true)
        setStatussnackbar(true)
        loadData()
    })
}

  const loadData = () => {
    setOpen(true)
    Promise.all([
      getAllTestimonials(), //0
    ])
    .then((data)=>{
      setOpen(false)
      let reversed = data[0].reverse()
      setTestimonials(reversed)
    })
  }

  return (
    <>
      { open && (
        <Backdrop
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={open}
          onClick={()=>setOpen(false)}>
          <CircularProgress color="inherit" />
        </Backdrop>
      )}


      <Snackbar open={statusSnackbar} autoHideDuration={6000} onClose={()=>setStatussnackbar(false)}>
        <Alert severity="success">La reseña fue enviada con éxito!</Alert>
      </Snackbar>

      {
        lang === 'es'? (
          <h2 className="head-text head-text-testimonial">Referencias & <span>Testimonios</span></h2>
        ) : (
          <h2 className="head-text head-text-testimonial">References & <span>Testimonials</span></h2>
        )
      }
      { statusLoader && (
        <Stack sx={{ width: '100%', color: 'grey.500' }} spacing={2} className="stack-loader">
          <LinearProgress color="secondary" />
        </Stack>
      )}
      {testimonials.length && (
        <>
          <div className="app__testimonial-item app__flex">
            <img src={ testimonials[currentIndex].image } alt={testimonials[currentIndex]._id} />
            <div className="app__testimonial-content">
              <p className="p-text">{testimonials[currentIndex].feedback}</p>
              <div>
                <h4 className="bold-text">
                  { !!testimonials[currentIndex].linkedin ? (
                    <a className='linkedin-link' rel="noreferrer" href={testimonials[currentIndex].linkedin} target="_blank">
                      <FontAwesomeIcon icon={faLinkedin} />
                      {testimonials[currentIndex].nombre}
                    </a>
                  ) : (
                    testimonials[currentIndex].nombre
                  )}
                </h4>
                <h5 className="p-text">{testimonials[currentIndex].company}</h5>
              </div>
            </div>
          </div>

          <div className="app__flex testimonials_buttons">
            <div className="app__testimonial-btns app__flex app_">
              <div className="app__flex" onClick={() => handleClick(currentIndex === 0 ? testimonials.length - 1 : currentIndex - 1)}>
                <HiChevronLeft />
              </div>

              <div className="app__flex" onClick={() => handleClick(currentIndex === testimonials.length - 1 ? 0 : currentIndex + 1)}>
                <HiChevronRight />
              </div>
            </div>

            <div className="buttons_add app_">
              <div className="button-box">
                <Button onClick={() => setModalresena(true)} className='button_enfasis'>
                  <span className="onhover">
                    🤔¿ Nos conocemos ?
                  </span>
                  <span className="offhover">
                    ¡ Deja una reseña ! 😀
                  </span>
                </Button>
                <Modal
                    open            = {modalResena}
                    onClose         = {() => setModalresena(false)}
                    aria-labelledby = "modal-modal-title"
                    aria-describedby="modal-modal-description">
                    <Box sx={boxStyles} className='app__modal-testimonial'>
                        <motion.div
                            className   = 'app--resena_form'
                            whileInView = {{ opacity: [0, 1] }}
                            transition  = {{ duration: 0.5, type: 'tween' }}>
                            <FormTestimonials 
                                key          = {`form`} 
                                setShowform  = { setModalresena } 
                                create       = { create }/>
                        </motion.div>
                    </Box>
                </Modal>

                <Modal
                    open            = {sucessFeedback}
                    onClose         = {() => setSuccessfeedback(false)}
                    aria-labelledby = "modal-feedback-title"
                    aria-describedby="modal-feedback-description">
                    <Box sx={boxStyles} className='app__modal-testimonial-greetings' onClick= {() => setSuccessfeedback(false)} >
                      <Typography variant="subtitle1" gutterBottom component="div" className='title-greetings'>
                        Gracias por tu reseña! 👍🤝
                      </Typography>
                      <Typography variant="subtitle1" gutterBottom component="div" className='body-greetings'>
                        Gracias por tu reseña! La revisaré personalmente y luego la publicaré luego de esto. Agradezco los comentarios de tu parte, los utilizaré con sabiduría para ser un mejor profesional y compañero de trabajo.💪
                      </Typography>
                      <Typography variant="subtitle1" gutterBottom component="div" className='bye-greetings'>
                        Que tengas un excelente dia!👋
                      </Typography>
                    </Box>
                </Modal>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default AppWrap(
  MotionWrap(Testimonial, 'app__testimonial'),
  'testimonios',
  'app__primarybg',
);