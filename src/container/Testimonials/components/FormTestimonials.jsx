import React , { useState, useRef, useEffect } from 'react'
import { TextField, Grid,  Button, Tooltip } from '@mui/material';
import {  motion } from 'framer-motion'
import { apiPath } from "../../../constants";
import './components.scss';

export const FormTestimonials = ({ create, setShowform }) => {
    const formProps                         = ['nombre','feedback','image','company','linkedin']
    const fileInput                         = useRef(null)
    const form                              = useRef(null)
    const [ nombre, setNombre ]             = useState('')
    const [ feedback, setFeedback ]         = useState('')
    const [ company, setCompany ]           = useState('')
    const [ linkedin, setLinkedin ]           = useState('')
    const [ imagePreview, setImagepreview ] = useState('')

    useEffect(() => {
        // setImagepreview(`http://localhost:3000/404.jpg`)
        setImagepreview(`${apiPath}404.jpg`)
    },[]);


    const handleSubmit = e => {
        e.preventDefault();
        const form     = e.target;
        const dataBody = getFormValues(form.elements,formProps)
        create(dataBody)
    }

    const handleChange = (event) => {
        let urlObject = URL.createObjectURL(event.target.files[0])
        setImagepreview(urlObject)
    }

    const handleClickFile = () => {
        fileInput.current.click()
    }

    const handleCancel = () => {
        setShowform(false)
    }

    const variants = {
        enter: (direction) => {
            return {
            x: direction > 0 ? 1000 : -1000,
            opacity: 0
            };
        },
        center: {
            zIndex: 1,
            x: 0,
            opacity: 1
        },
        exit: (direction) => {
            return {
            zIndex: 0,
            x: direction < 0 ? 1000 : -1000,
            opacity: 0
            };
        }

        };

    return (
        <motion.div
            variants   = {variants}
            initial    = "enter"
            animate    = "center"
            exit       = "exit"
            className='app__form-container'
            transition = {{
            x          : { type: "spring", stiffness: 300, damping: 30 },
            opacity    : { duration: 0.2 }
            }}>
                <form ref={ form} onSubmit={handleSubmit}>
                    <Grid container  spacing={1} columns={8} className='app_modalbox-testimonial'>
                        <Grid item md={3} className='form-box'>
                            <div className="app__files-container">
                                { imagePreview && (
                                    <div className="app__row-image">
                                        <img src={ imagePreview } alt={`preview-about`} />
                                    </div>
                                )}
                                <div className="upload">
                                    <input
                                        accept   = "image/*"
                                        style    = {{ display: 'none' }}
                                        id       = "image"
                                        name     = "image"
                                        ref      = {fileInput}
                                        onChange = {handleChange}
                                        type     = "file"/>
                                        <label htmlFor="image">
                                            <Button onClick={handleClickFile} variant="outlined" size="small">
                                                Adjuntar Imagen
                                            </Button>
                                        </label>
                                </div>
                            </div>
                        </Grid>
                        <Grid item container md={5}>
                            <Grid item md={12} className='form-box'>
                                <TextField 
                                    fullWidth
                                    size    = "small"
                                    name = "nombre"
                                    value={ nombre }
                                    label   = "¿ Cuál es tu nombre ?"
                                    onChange = {(e)=>{setNombre(e.target.value)}}
                                    variant = "outlined" />
                            </Grid>
                            <Grid item md={12} className='form-box'>
                                    <TextField 
                                        fullWidth
                                        size    = "small"
                                        name = "linkedin"
                                        value={ linkedin }
                                        label   = "Linkedin"
                                        onChange = {(e)=>{setLinkedin(e.target.value)}}
                                        variant = "outlined" />
                            </Grid>
                            <Grid item md={12} className='form-box'>
                                <TextField 
                                    fullWidth
                                    size     = "small"
                                    name     = "company"
                                    value    = { company }
                                    label    = "¿ Cual es tu Profesión ?"
                                    onChange = {(e)=>{setCompany(e.target.value)}}
                                    variant  = "outlined" />
                            </Grid>
                            <Grid item md={12} className='form-box'>
                                <TextField 
                                    fullWidth
                                    size    = 'small'
                                    name    = "feedback"
                                    multiline
                                    rows    = {4}
                                    label    = "Reseña"
                                    value   = { feedback }
                                    onChange = {(e)=>{setFeedback(e.target.value)}}
                                    variant =  "outlined" />
                            </Grid>
                            <Grid className='app__submit-row' item md={12}>
                                <div className="app__buttons-box">
                                    <Tooltip title="Cancelar">
                                        <Button 
                                            variant = 'outlined' 
                                            onClick = { handleCancel }>
                                            Cancelar
                                        </Button>
                                    </Tooltip>
                                    <Tooltip title="Eliminar">
                                        <Button type='submit' variant='outlined'>
                                            Crear
                                        </Button>
                                    </Tooltip>
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                </form>
        </motion.div>
    )
}

const getFormValues = (form,props) => {
    const formData = new FormData();

    props.forEach((prop,index)=>{
        if(prop !== 'image'){
            let dataValue = form[prop].value
            if(dataValue)
                formData.append(prop,dataValue)
        } else {
            let file = form[prop].files[0]
            if(file)
                formData.append(prop,file)
        }
    })

    return formData
}