import React, { useState, useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { AiFillEye, AiFillGithub } from 'react-icons/ai';
import { motion } from 'framer-motion';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import { AppWrap, MotionWrap } from '../../wrapper';
import './Work.scss';
import { callWorks } from '../../store/dataState';


const Work = () => {
  const works           = useSelector((state) => state.data.works);
  const lang            = useSelector((state) => state.lang.lang);
  const [animateCard, ] = useState({ y: 0, opacity: 1 });
  const [open, setOpen] = useState(false);
  const dispatch        = useDispatch()

  const loadData = useCallback(() => {
    dispatch(callWorks(lang))
  },[dispatch,lang])

  useEffect(() => {
    loadData()
  }, [loadData]);

  return (
    <>
      { open && (
        <Backdrop
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={open}
          onClick={()=>setOpen(false)}>
          <CircularProgress color="inherit" />
        </Backdrop>
      )}
      {
        lang === 'es'? (
          <h2 className="head-text">Portafolio <span>Creativo</span></h2>
        ) : (
            <h2 className="head-text">Creative  <span>briefcase</span></h2>
        )
      }
      <motion.div
        animate={animateCard}
        transition={{ duration: 0.5, delayChildren: 0.5 }}
        className="app__work-portfolio">
        {works.map((work, index) => (
          <div className="app__work-item app__flex" key={`${work._id}`}>
            <div
              className="app__work-img app__flex">
              <img src={work.image} alt={work.titulo} />

              <motion.div
                whileHover = {{ opacity: [0, 1] }}
                transition = {{ duration: 0.25, ease: 'easeInOut', staggerChildren: 0.5 }}
                className  = "app__work-hover app__flex">
                  { work.link && (
                    <a 
                      href   = {work.link} 
                      target = "_blank" 
                      rel    = "noreferrer">
                      <motion.div
                        whileInView = {{ scale: [0, 1] }}
                        whileHover  = {{ scale: [1, 0.90] }}
                        transition  = {{ duration: 0.25 }}
                        className   = "app__flex">
                        <AiFillEye />
                      </motion.div>
                    </a>
                  )}
                  { work.code_link &&  (
                    <a href={work.code_link} target="_blank" rel="noreferrer">
                      <motion.div
                        whileInView = {{ scale: [0, 1] }}
                        whileHover  = {{ scale: [1, 0.90] }}
                        transition  = {{ duration: 0.25 }}
                        className   = "app__flex"
                      >
                        <AiFillGithub />
                      </motion.div>
                    </a>
                  )}
              </motion.div>
            </div>

            <div className="app__work-content app__flex">
              <h4 className="bold-text">{work.titulo}</h4>
              <p className="p-text" style={{ marginTop: 10 }}>{work.description}</p>
{/* 
              <div className="app__work-tag app__flex">
                <p className="p-text">{work.tags[0]}</p>
              </div> */}
            </div>
          </div>
        ))}
      </motion.div>
    </>
  );
}

export default AppWrap(
  MotionWrap(Work, 'app__works'),
  'trabajo',
  'app__primarybg',
);