import React from 'react'
import { Navbar } from '../../components';
import { 
    About,
    Footer,
    Header,
    Skills,
    Work,
    Testimonials } from '../../container';


export const Main = () => {
  return (
    <>
        <Navbar />
        <Header />
        <About />
        <Work />
        <Skills />
        <Testimonials />
        <Footer />
    </>
  )
}
