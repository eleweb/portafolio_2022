import React, { useState, useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { motion } from 'framer-motion';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import { AppWrap, MotionWrap } from '../../wrapper';
import './About.scss';
import { callAbouts } from '../../store/dataState'

const About = () => {
  const abouts = useSelector((state) => state.data.abouts);
  const lang = useSelector((state) => state.lang.lang);
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch()

  const loadData = useCallback(() => {
    dispatch(callAbouts(lang))
  },[dispatch,lang])

  
  useEffect(() => {
    loadData()
  }, [loadData]);


  return (
    <>
      { open && (
        <Backdrop
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={open}
          onClick={()=>setOpen(false)}>
          <CircularProgress color="inherit" />
        </Backdrop>
      )}
      
      {
        lang === 'es'? 
        (
          <h2 className="head-text">Un poco  <span>sobre mí</span></h2>
        ) : 
        (
            <h2 className="head-text">A little bit  <span>about me</span></h2>
        )
      }

      <div className="app__profiles">
        {abouts.map((about, index) => (
          <motion.div
            whileInView = {{ opacity: 1 }}
            whileHover  = {{ scale: 1.1 }}
            transition  = {{ duration: 0.5, type: 'tween' }}
            className   = "app__profile-item"
            key         = {about._id}
          >
            <img src={about.image} alt={about.title} />
            <h2 className="bold-text" style={{ marginTop: 20 }}>{about.titulo}</h2>
            <p className="p-text" style={{ marginTop: 10 }}>{about.description}</p>
          </motion.div>
        ))}
      </div>
    </>
  );
};

export default AppWrap(
  MotionWrap(About, 'app__about'),
  'acerca',
  'app__whitebg',
);