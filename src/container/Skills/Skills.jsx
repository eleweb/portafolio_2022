import React, { useState, useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { motion } from 'framer-motion';
import ReactTooltip from 'react-tooltip';
import { Modal, Typography, Box} from '@mui/material';
import { AppWrap, MotionWrap } from '../../wrapper';
import './Skills.scss';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import { callSkills, callExperiences, callElearning } from '../../store/dataState';

const boxStyles = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '90vw',
  maxWidth: '95vw',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  borderRadius: '5px',
  p: 4,
};

const Skills = () => {
  const experiences                      = useSelector((state) => state.data.experiences);
  const skills                           = useSelector((state) => state.data.skills);
  const elearnings                       = useSelector((state) => state.data.elearning);
  const lang                             = useSelector((state) => state.lang.lang);
  const [ selectedItem, setSelecteditem] = useState(null)
  const [modalToggle, setModalToggle]    = useState(false);
  const [open, setOpen]                  = useState(false);
  const dispatch                         = useDispatch()

  const loadData = useCallback(() => {
    dispatch(callSkills(lang))
    dispatch(callExperiences(lang))
    dispatch(callElearning(lang))
  },[dispatch,lang])

  useEffect(() => {
    loadData()
  }, [loadData]);

  const clickBrand = (item) => () => {
    setSelecteditem(item)
    setModalToggle(!modalToggle)
  }
  
  return (
    <>
      { open && (
        <Backdrop
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={open}
          onClick={()=>setOpen(false)}>
          <CircularProgress color="inherit" />
        </Backdrop>
      )}    
    {
      lang === 'es'? (
        <h2 className="head-text">Habilidades & Experiencias</h2>
      ) : (
        <h2 className="head-text">Skills & Work Experiences</h2>
      )
    }
      <div className="app__skills-container">
        <motion.div className="app__skills-list">
          {skills.map((skill) => (
            <motion.div key={`${skill._id}-container`}>
              <motion.div
                whileInView = {{ opacity: [0, 1] }}
                transition  = {{ duration: 0.5 }}
                className   = "app__skills-item app__flex"
                data-tip
                data-for={`${skill._id}-tooltip`}
                key         = {`${skill._id}-container`}>
                <div
                  className="app__flex"
                  style={{ backgroundColor: skill.bgColor }}>
                  <img src={skill.image} alt={skill.titulo} />
                </div>
              </motion.div>
              <ReactTooltip
                  key        = {`${skill._id}-tooltip-skill`}
                  effect     = "float"
                  arrowColor = "#fff"
                  id={`${skill._id}-tooltip`}
                  className  = "skills-tooltip">
                  <p className="p-text">{skill.description}</p>
              </ReactTooltip>
            </motion.div>
          ))}
        </motion.div>
        <div className="app__skills-exp">
          {experiences.map((experience,experienceIndex) => (
            <motion.div
              className = "app__skills-exp-item"
              key       = {`${experience._id}-year-item`}>
              <div className="app__skills-exp-year">
                <p className="bold-text">{experience.start}</p>
              </div>
              <motion.div className="app__skills-exp-works">
                  <motion.div
                    data-tip
                    whileInView = {{ opacity: [0, 1] }}
                    transition  = {{ duration: 0.5 }}
                    className   = "app__skills-exp-experience"
                    data-for    = {experienceIndex}
                    key         = {`${experienceIndex}-experience`}>
                    <h4 className = "bold-text">{experience.cargo}</h4>
                    <p className  = "p-text">{experience.nombre}</p>
                    <p className  = "p-text">{experience.description}</p>
                  </motion.div>
              </motion.div>
            </motion.div>
          ))}
        </div>
      </div>





      <div className="app__testimonial-brands app__flex">
        {elearnings.map((brand) => (
          <motion.div
            onClick     = {clickBrand(brand)} 
            whileInView = {{ opacity: [0, 1] }}
            transition  = {{ duration: 0.5, type: 'tween' }}
            key         = {brand._id}>
            <img src={brand.image} alt={brand.titulo} />
          </motion.div>
        ))}

          { selectedItem && (
            <Modal
              open            = {modalToggle}
              onClose         = {() => setModalToggle(false)}
              aria-labelledby = "modal-modal-title"
              aria-describedby="modal-modal-description">
                  <Box sx={boxStyles}>
                    <Typography id="keep-mounted-modal-title" variant="h6" component="h2">
                      {selectedItem.titulo}
                    </Typography>
                    <motion.div
                      onClick     = {() => setModalToggle(false)}
                      className   = 'app__img'
                      whileInView = {{ opacity: [0, 1] }}
                      transition  = {{ duration: 0.5, type: 'tween' }}
                      key         = {selectedItem._id}>
                        <img src={selectedItem.image} alt={selectedItem.titulo} />
                    </motion.div>
                  </Box>
            </Modal>
          )}
      </div>
    </>
  );
}


export default AppWrap(
  MotionWrap(Skills, 'app__skills'),
  'habilidades',
  'app__whitebg',
);