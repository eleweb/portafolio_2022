import React from 'react'
import { Navbar } from '../../components'
import { Admin } from '../../container'

export const MainAdmin = () => {
  return (
    <>
        <Navbar />
        <Admin />
    </>
  )
}
