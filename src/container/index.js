import Skills from './Skills/Skills';
import About from './About/About';
import Work from './Work/Work';
import Testimonials from './Testimonials/Testimonials';
import Footer from './Footer/Footer';
import  Admin  from "./Admin/Admin";
export  { Main } from './Main/Main';
export { Header } from "./Header/Header";
export { MainAdmin } from "./MainAdmin/MainAdmin";

export {
    Skills,
    About,
    Work,
    Testimonials,
    Footer,
    Admin
};