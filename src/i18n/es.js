export const es = {
    contact     : 'Contacto',
    home        : 'Inicio',
    skills      : 'Habilidades',
    about       : 'Acerca',
    works       : 'Proyectos',
    testimonials: 'Referencias',
    download_cv: 'Descargar CV',
    hi:'Hola mi nombre es'
}